# ElevageDeVolaille de Ga�tan Delacroix

## Description du Main

 - Initialisation de l'�leveur et d'un Scanner.
 - V�rification de l'existence du dossier "Ressources", dans le cas o� il n'existe pas, il est ainsi cr��.
 - Remise � 0 du document txt contenant les identifiants des diff�rentes volailles de la liste de volaille  de l'�leveur � chaque lancement du programme.
 - Appel � la m�thode qui permet d'afficher et d'utiliser le Menu.

### Description du Menu

1 - Ajouter une/des volaille(s)

	- Permet de choisir le nombre, le type et le poids des volailles � ajouter.
	- Cependant il existe des restrictions: pas plus de 5 poulets, 4 canards, 3 paons et 7 volailles en tout, par ajout

2 - Modifier poids abattage

	- Permet d'initialiser/modifier le poids d'abattage des poulets ou des canards 
	- ATTENTION: ne pas �crire le poids sous cette forme : "x.x" , s'il s'agit d'un nombre d�cimal (cel� fait planter le programme)
	- Il faut donc privil�gier la forme : "x,x" ou saisir un entier.

3 - Modifier prix du jour

	- Permet d'initialiser/modifier le prix au kilo du jour des poulets ou des canards 
	- ATTENTION: ne pas �crire le prix sous cette forme : "x.x" , s'il s'agit d'un nombre d�cimal (cel� fait planter le programme)
	- Il faut donc privil�gier la forme : "x,x" ou saisir un entier.

4 - Modifier le poids d'une volaille

	- Permet de modifier le poids d'une volaille existante de type Poulet ou Canard
	- ATTENTION: ne pas �crire le poids sous cette forme : "x.x", s'il s'agit d'un nombre d�cimal (cel� fait planter le programme)
	- Il faut donc privil�gier la forme : "x,x" ou saisir un entier.

5 - Voir le nombre de volailles par type

	- Permet d'afficher le nombre exact de volailles de l'�levage selon leur type (Poulet, Canard et Paon)

6 - Voir le total de prix des volailles abattables

	- Permet d'afficher le nombre de volailles abattables selon le type (Poulet, Canard) 
	- Permet d'afficher le Chiffre d'affaires r�alisable via la vente des poulets et canards abattables
	- Le calcul du prix se fait en fonction du poids des volailles concern�es et du prix au kilo du jour

7 - Vendre une volaille

	- Permet de vendre une volaille (Poulet ou Canard)
	- La volaille vendue est donc retirer de la liste de volailles de l'�leveur

8 - Rendre un paon au parc

	- Permet de rendre un paon au parc
	- Le paon rendu est donc retirer de la liste de volailles de l'�leveur

Q - Quitter

	- Affiche un message indiquant que l'on a quitt� le programme


#### Ressources

Le dossier "Ressources" est g�n�r� automatiquement s'il est inexistant sur la machine utilisant le programme (Comme d�crit plus haut dans la section "Main")
Ce dossier Ressources contient un document txt regroupant l'ensemble des identifiants des volailles appartenant � la liste de volailles de l'�leveur.
A chaque ajout de volaille (via l'onglet 1 du Menu) l'identifiant des volailles ajout�es est stock� automatiquement dans le fichier txt "IdentifiantVolaille.txt"
Le dossier "Ressources" est remis � z�ro automatiquement � chaque lancement du programme.






