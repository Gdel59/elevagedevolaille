package fr.afpa.ihm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

import fr.afpa.entites.Eleveur;
import fr.afpa.services.ServiceMenu;

public class Main {

	public static void main(String[] args) {
		
		Eleveur eleveurDeVolaille = new Eleveur();
		
		Scanner in = new Scanner(System.in);
		try {
			if (!new File("Ressources").exists())
				new File("Ressources").mkdir();
			FileWriter fw = new FileWriter(
					"Ressources\\IdentifiantVolaille.txt",false);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.close();
		} catch (Exception e) {
			System.out.println("Erreur" + e);
		}

			ServiceMenu.choixMenu(eleveurDeVolaille, in);
	
		 
		}
	}


