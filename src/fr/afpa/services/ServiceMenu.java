package fr.afpa.services;

import java.util.Map;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;



import fr.afpa.controles.ControleCanard;
import fr.afpa.controles.ControlePoulet;
import fr.afpa.entites.Canard;
import fr.afpa.entites.Eleveur;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.entites.Volaille;
import fr.afpa.entites.VolailleAAbattre;

public class ServiceMenu {
	
	private final static int compteurVolaille = 7;

	/**
	 * Méthode qui permet d'afficher le menu et de choisir les options du menu voulues
	 * @param eleveurDeVolaille eleveur de volaille
	 * @param in Scanner
	 */
	
	public static void choixMenu(Eleveur eleveurDeVolaille, Scanner in) {
		String choix = "";
		
		while(!choix.equals("Q")) {
			
			System.out.println("--------------------------------------------------");	
			System.out.println("                       MENU ");
			System.out.println("--------------------------------------------------");
			System.out.println("1 - Ajouter une/des volaille(s)");
			System.out.println("2 - Modifier poids abattage");
			System.out.println("3 - Modifier prix du jour");
			System.out.println("4 - Modifier le poids d'une volaille");
			System.out.println("5 - Voir le nombre de volailles par type");
			System.out.println("6 - Voir le total de prix des volailles abattables");
			System.out.println("7 - Vendre une volaille");
			System.out.println("8 - Rendre un paon au parc");
			System.out.println("--------------------------------------------------");
			System.out.println("Q - Quitter");
			System.out.println("--------------------------------------------------");	
			
			choix = in.nextLine();
		
		switch (choix) {

		case "1":
			ajouterVolaille(in, eleveurDeVolaille);
			break;

		case "2": affichageMethodeModifPoidsAbattage(in);
			break;

		case "3":affichageMethodeModifPrixDuJour(in);
			break;

		case "4":
			String id;
			double poidsVolaille;
			System.out.println(eleveurDeVolaille.getListeDeVolaille());
			do {
			System.out.println("Veuillez saisir l'identifiant de la volaille dont vous voulez modifier le poids");
			 id = in.nextLine();}
			
			while(!eleveurDeVolaille.getListeDeVolaille().containsKey(id));
			
			do {
			System.out.println("Veuillez saisir le nouveau poids");
			 poidsVolaille = in.nextDouble();in.nextLine();}
			while(!ControlePoulet.verifPoidsPoulet(poidsVolaille));
			
			methodeModifPoidsVolaille(eleveurDeVolaille, id, poidsVolaille);
			break;

		case "5": voirNombreVolailleParType(eleveurDeVolaille);
			break;

		case "6": totalPrixVolailleabattables(eleveurDeVolaille);
			break;

		case "7":
			String idVolaille;
			do {
				System.out.println("Veuillez saisir l'identifiant de la volaille que vous voulez vendre");
			idVolaille = in.nextLine();
			}
			while(!eleveurDeVolaille.getListeDeVolaille().containsKey(idVolaille));
			
			vendreUneVolaille(eleveurDeVolaille, idVolaille);
			break;

		case "8":
			String idPaon;
			do {
				System.out.println("Veuillez saisir l'identifiant du paon que vous voulez rendre au parc");
				idPaon = in.nextLine();
			}
			while(!eleveurDeVolaille.getListeDeVolaille().containsKey(idPaon));
			
			rendreUnPaon(eleveurDeVolaille, idPaon);
			break;

		case "Q": System.out.println("Vous avez quitté le programme");
			break;

		default:
			System.out.println("Veuillez saisir un caractère valide !");
			break;
		}
		
		}
	}
		
		/**
		 * Methode permettant d'ajouter une ou plusieurs volailles de chaque espèce dans la liste de volaille de l'éleveur
		 *  tout en respectant les règles métiers
		 *  ainsi que mémoriser les identifiants de chaque volaille dans un fichier txt
		 * @param in Scanner
		 * @param eleveur Eleveur de volaille 
		 */
	private static void ajouterVolaille(Scanner in, Eleveur eleveur) {
		
		
	
			System.out.println("Combien de poulets voulez-vous ajouter ?");
			int nbrPoulet = in.nextInt();in.nextLine();
			while (nbrPoulet<0 || nbrPoulet>5) {
				System.out.println("Nombre saisi incorrect, veuillez saisir un nombre entre 0 et 5.");
				nbrPoulet = in.nextInt();in.nextLine();
			}
			System.out.println("Combien de canards voulez-vous ajouter ?");
		    int nbrCanard = in.nextInt();in.nextLine();
		    while (nbrCanard<0 || nbrCanard>4 || (nbrCanard + nbrPoulet)>compteurVolaille) {
		    	System.out.println("On ne peut ajouter plus de 7 volailles en une seule fois, veuillez saisir un autre nombre.");
		    	nbrCanard = in.nextInt();in.nextLine();
		    }
			System.out.println("Combien de paons voulez-vous ajouter ?");
			int nbrPaon = in.nextInt();in.nextLine();
			while (nbrPaon<0 || nbrPaon>3 || (nbrCanard + nbrPoulet + nbrPaon)>compteurVolaille) {
				System.out.println("On ne peut ajouter plus de 7 volailles en une seule fois, veuillez saisir un autre nombre.");
				nbrPaon = in.nextInt();in.nextLine();
		}
			
			for (int i=1; i<=nbrPoulet; i++) {
				double poidsPoulet;
				do {
				System.out.println("Quel est le poids du poulet "+ i + " ?");
				 poidsPoulet = in.nextDouble();in.nextLine();}
				
				while(!ControlePoulet.verifPoidsPoulet(poidsPoulet));
				
				Volaille poulet = new Poulet(poidsPoulet);
				eleveur.getListeDeVolaille().put(poulet.getNumeroIdentifiant(), poulet);
				
				try {
					FileWriter fw = new FileWriter(
							"Ressources\\IdentifiantVolaille.txt",true);
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(poulet.getNumeroIdentifiant());
					bw.newLine();
					bw.close();
				} catch (Exception e) {
					System.out.println("Erreur" + e);
				}
			}
			
			for (int j=1; j<=nbrCanard; j++) {
				double poidsCanard;
				do {
				System.out.println("Quel est le poids du canard "+ j + " ?");
				 poidsCanard = in.nextDouble();in.nextLine();}
				
				while(!ControleCanard.verifPoidsCanard(poidsCanard));
				
				Volaille canard = new Canard(poidsCanard);
				eleveur.getListeDeVolaille().put(canard.getNumeroIdentifiant(), canard);
				
				try {
					FileWriter fw = new FileWriter(
							"Ressources\\IdentifiantVolaille.txt",true);
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(canard.getNumeroIdentifiant());
					bw.newLine();
					bw.close();
				} catch (Exception e) {
					System.out.println("Erreur" + e);
				}
			}
			
			for (int k=0; k<nbrPaon; k++) {
				Volaille paon = new Paon();
				eleveur.getListeDeVolaille().put(paon.getNumeroIdentifiant(),paon);
				
				try {
					FileWriter fw = new FileWriter(
							"Ressources\\IdentifiantVolaille.txt",true);
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(paon.getNumeroIdentifiant());
					bw.newLine();
					bw.close();
				} catch (Exception e) {
					System.out.println("Erreur" + e);
				}
					
			}
	
System.out.println(eleveur.getListeDeVolaille());
			
				
	}
	
/**
 * Méthode permettant d'initialiser/modifier le poids d'abattage des poulets et des canards
 * @param in Scanner
 */
	private static void affichageMethodeModifPoidsAbattage( Scanner in) {

		System.out.println("Veuillez choisir le type de Volaille dont vous voulez modifier le poids d'abattage:");
		System.out.println();
		System.out.println("1 - Poulet");
		System.out.println("2 - Canard");
		String choix2 = in.nextLine();

		
			switch (choix2) {

			case "1":
				double nouveauPoidsAbattagePoulet;
				do {
				System.out.println("Veuillez saisir le nouveau poids d'abattage des poulets (en kilogramme)");
				 nouveauPoidsAbattagePoulet = in.nextDouble();in.nextLine();}
				
				while(!ControlePoulet.verifPoidsPoulet(nouveauPoidsAbattagePoulet));
				
				new Poulet().modifPoidsAbattage(nouveauPoidsAbattagePoulet);

				System.out.println(
						"Le nouveau poids d'abattage des poulets est de " + nouveauPoidsAbattagePoulet + " kg");
				System.out.println();
					
				
				break;
				

			case "2":
				double nouveauPoidsAbattageCanard;
				do {
				System.out.println("Veuillez saisir le nouveau poids d'abattage des canards (en kilogramme)");
				 nouveauPoidsAbattageCanard = in.nextDouble();in.nextLine();}
				 
				 while(!ControleCanard.verifPoidsCanard(nouveauPoidsAbattageCanard));
				 
				new Canard().modifPoidsAbattage(nouveauPoidsAbattageCanard);

				System.out.println(
						"Le nouveau poids d'abattage des canards est de " + nouveauPoidsAbattageCanard + " kg");
				System.out.println();break;
				
				
			default: System.out.println("Veuillez taper 1 ou 2 !");
				break;
			}

		}
	
	
	/**
	 * Méthode qui permet d'initialiser/modifier le prix au kilo du poulet et du canard
	 * @param in Scanner
	 */
	private static void affichageMethodeModifPrixDuJour(Scanner in) {
		
		System.out.println("Veuillez choisir le type de Volaille dont vous voulez modifier le prix au kilo du jour:");
		System.out.println();
		System.out.println("1 - Poulet");
		System.out.println("2 - Canard");
		String choix2 = in.nextLine();
		
		switch (choix2) {

		case "1":
			double nouveauPrixDuJourPoulet;
			do {
			System.out.println("Veuillez saisir le nouveau prix au kilo du jour des poulets ");
			 nouveauPrixDuJourPoulet = in.nextDouble();in.nextLine();}
			
			while(!ControlePoulet.verifPrixPoulet(nouveauPrixDuJourPoulet)); 
				
			new Poulet().modifPrixDuJour(nouveauPrixDuJourPoulet);

			System.out.println(
					"Le nouveau prix au kilo du jour des poulets est de " + nouveauPrixDuJourPoulet + " €");
			System.out.println();break;
			

		case "2":
			double nouveauPrixDuJourCanard;
			do {
			System.out.println("Veuillez saisir le nouveau prix au kilo du jour des canards ");
			 nouveauPrixDuJourCanard = in.nextDouble();in.nextLine();}
			
			while(!ControleCanard.verifPrixCanard(nouveauPrixDuJourCanard));
			
			new Canard().modifPrixDuJour(nouveauPrixDuJourCanard);

			System.out.println(
					"Le nouveau prix au kilo du jour des canards est de " + nouveauPrixDuJourCanard + " €");
			System.out.println();break;
			
			
		default: System.out.println("Veuillez taper 1 ou 2 !");
			break;
		}

		
	}
	
	/** 
	 * Méthode qui permet de modifier le poids d'une volaille de type Canard ou de type Poulet
	 * @param eleveur eleveur de volaille
	 * @param identifiantVolaille identifiant d'un poulet ou d'un canard
	 * @param poids poids d'un poulet ou d'un canard
	 */
	private static void methodeModifPoidsVolaille(Eleveur eleveur, String identifiantVolaille, double poids) {
		
		
		Volaille volaille = eleveur.getListeDeVolaille().get(identifiantVolaille);
		
	if (volaille instanceof Poulet) {
		
		((Poulet) volaille).setPoids(poids);
	}
	
	if (volaille instanceof Canard) {
		((Canard) volaille).setPoids(poids);
	}
		
	}
	
	/**
	 * Méthode qui permet d'afficher le nombre exact de poulet / canard / paon de l'élevage
	 * @param eleveur eleveur de volaille
	 */
	
	private static void voirNombreVolailleParType(Eleveur eleveur) {
		int compteurPoulet = 0;
		int compteurCanard = 0;
		int compteurPaon = 0;
		
		for (Map.Entry<String, Volaille> volaille: eleveur.getListeDeVolaille().entrySet() ){
			
			if (volaille.getValue() instanceof Poulet) {
				compteurPoulet++;
			}
			
			if (volaille.getValue() instanceof Canard) {
				compteurCanard++;
			}
			
			if (volaille.getValue() instanceof Paon) {
				compteurPaon++;
			}
			
			
		}
		
		System.out.println("Il y a "+ compteurPoulet + " poulet(s) dans l'élevage");
		System.out.println("Il y a "+ compteurCanard + " canard(s) dans l'élevage");
		System.out.println("Il y a "+ compteurPaon + " paon(s) dans l'élevage");
		
	}
	
	/**
	 * Méhode qui permet de déterminer quelles volailles sont abattables 
	 * et qui calcule le montant total des poulets abattables et des canards abattables
	 * @param eleveur eleveur de volaille
	 */
	
	private static void totalPrixVolailleabattables(Eleveur eleveur) {
		double totalPrixPouletAbattable = 0;
		double totalPrixCanardAbattable = 0;
		int compteurPouletAbattable = 0;
		int compteurCanardAbattable = 0;
		double totalPrixVolailleAbattable = 0;
		
		for (Map.Entry<String, Volaille> volaille: eleveur.getListeDeVolaille().entrySet()) {
			
			if (volaille.getValue() instanceof Poulet) {
				if (((Poulet) volaille.getValue()).getPoids() >= Poulet.getPoidsAbattage()){
					
					compteurPouletAbattable++;
					totalPrixPouletAbattable += ((Poulet) volaille.getValue()).getPoids() * Poulet.getPrixAuKilos();	
				}
				
			}
			
			if (volaille.getValue() instanceof Canard) {
				if (((Canard) volaille.getValue()).getPoids() >= Canard.getPoidsAbattage()) {
					
					compteurCanardAbattable++;
					totalPrixCanardAbattable += ((Canard) volaille.getValue()).getPoids() * Canard.getPrixAuKilos();
				}
				
				
			}
			
			totalPrixVolailleAbattable = totalPrixCanardAbattable+totalPrixPouletAbattable;
			
		}
		
		System.out.println("Il y a un total de " + compteurPouletAbattable + " poulet(s) abattable(s) pour un montant de : "+ totalPrixPouletAbattable + " €.");
		System.out.println("Il y a un total de " + compteurCanardAbattable + " canard(s) abattable(s) pour un montant de : "+ totalPrixCanardAbattable + " €.");
		System.out.println();
		System.out.println("Le prix total des volailles abattables s'élève à " + totalPrixVolailleAbattable + " €.");
		
	}
	
	/**
	 * Méthode permettant de choisir une volaille à vendre (canard ou poulet) en parcourant la liste de Volaille
	 * @param eleveur eleveur de volaille
	 * @param identifiantVolaille identifiant d'un poulet ou d'un canard
	 */
	
	private static void vendreUneVolaille(Eleveur eleveur, String identifiantVolaille) {
		
		if (eleveur.getListeDeVolaille().get(identifiantVolaille) instanceof VolailleAAbattre) {
			eleveur.getListeDeVolaille().remove(identifiantVolaille);}
		
	else {
		System.out.println("L'identifiant saisi n'existe pas, ou peut être avez-vous essayé de vendre un paon..");
	}
		
	}
	
	/**
	 * Méthode qui permet de rendre un paon au parc, c'est à dire d'enlever un paon de la liste de volaille de l'éleveur
	 * @param eleveur eleveur de volaille
	 * @param identifiantPaon identifiant d'un paon
	 */
	
	private static void rendreUnPaon(Eleveur eleveur, String identifiantPaon) {
		
				
		if (eleveur.getListeDeVolaille().get(identifiantPaon) instanceof Paon) {
				eleveur.getListeDeVolaille().remove(identifiantPaon);}
		
		else {
			System.out.println("L'identifiant saisi n'existe pas, ou peut être avez-vous essayé de rendre un poulet/un canard au parc..");
		}
		
				
			
		
		
	}

	}


