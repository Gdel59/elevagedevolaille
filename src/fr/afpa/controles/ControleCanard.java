package fr.afpa.controles;

public class ControleCanard {

	/**
	 * Méthode qui permet de vérifier la valeur du prix au kilo des canards
	 * @param prix prix au kilo du jour (Canard)
	 * @return vrai si le prix est compris dans la limite instaurée dans la méthode sinon renvoie message "d'erreur"
	 */
	
	public static boolean verifPrixCanard(double prix) {
		
		
			if ( prix>3.80 && prix<20.50) {
				return true;
			}
			else System.out.println("Le prix du canard au kilo doit se situer entre 3.80€ et 20.50€ selon la qualité de votre produit");
			return false;
		}
	
	/**
	 * Méthode qui permet de vérifier la valeur du poids d'un canard
	 * @param poids poids d'un canard
	 * @return vrai si le poids est compris dans la limite instaurée dans la méthode sinon renvoie message "d'erreur"
	 */
	
	public static boolean verifPoidsCanard(double poids) {
		
		if (poids>=0 && poids<20) {
			
			return true;
			
		}
		else System.out.println("Le poids d'un canard ne peut être négatif, être égal à 0 ou dépasser les 20 kilos");
		return false;
	}
	
}
