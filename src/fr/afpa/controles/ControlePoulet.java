package fr.afpa.controles;

public class ControlePoulet {
	
	/**
	 * Méthode qui permet de vérifier la valeur du prix au kilo des poulets 
	 * @param prix prix au kilo du jour des poulets
	 * @return vrai si le prix est compris dans la limite instaurée dans la méthode sinon renvoie un message "d'erreur"
	 */
	
	public static boolean verifPrixPoulet(double prix) {
		
		
			if (prix>1.35 && prix<16.90) {
				return true;
			}
			System.out.println("Le prix du poulet au kilo doit se situer entre 1.35€ et 16.90€ selon la qualité de votre produit");
			return false;
		}
		
	
	/**
	 * méthode qui permet de vérifier la valeur du poids d'un poulet
	 * @param poids poids d'un poulet
	 * @return vrai si le poids est compris dans la limite fixée dans la méthode sinon renvoie un message "d'erreur"
	 */
	
	public static boolean verifPoidsPoulet(double poids) {
		
		if (poids>=0 && poids<20) {
			
			return true;
			
		}
		else System.out.println("Le poids d'un poulet ne peut être négatif, être égal à 0 ou dépasser les 20 kilos");
		return false;
	}
		
		
	}


