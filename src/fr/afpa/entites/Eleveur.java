package fr.afpa.entites;

import java.util.HashMap;
import java.util.Map;

public class Eleveur {
	
private	Map<String,Volaille> listeDeVolaille;

	public Eleveur() {
		super();
		setListeDeVolaille(new HashMap<String, Volaille>());
	}

	public Map<String,Volaille> getListeDeVolaille() {
		return listeDeVolaille;
	}

	public void setListeDeVolaille(Map<String,Volaille> listeDeVolaille) {
		this.listeDeVolaille = listeDeVolaille;
	}
	
	

}
