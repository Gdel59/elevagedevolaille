package fr.afpa.entites;

public abstract class VolailleAAbattre extends Volaille {

	private double poids;

	public VolailleAAbattre() {
		super();

	}

	public VolailleAAbattre(double poids) {
		super();
		this.poids = poids;
	}

	public double getPoids() {
		return poids;
	}

	public void setPoids(double poids) {
		this.poids = poids;
	}

	public boolean modifPoids(double nouveauPoids) {

		if (nouveauPoids > 0) {
			this.setPoids(nouveauPoids);

			return true;
		}
		System.out.println("Le poids saisi est incorrect !");
		return false;
	}

	public abstract boolean modifPoidsAbattage(double nouveauPoidsAbattage);

}
