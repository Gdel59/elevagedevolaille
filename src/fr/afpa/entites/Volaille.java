package fr.afpa.entites;

public abstract class Volaille {
	
	protected String numeroIdentifiant;
	protected static int idAutoIncrement;
	
	
	public String getNumeroIdentifiant() {
		return numeroIdentifiant;
	}
	
	public void setNumeroIdentifiant(String numeroIdentifiant) {
		this.numeroIdentifiant = numeroIdentifiant;
	}
	

}
