package fr.afpa.entites;

public class Canard extends VolailleAAbattre implements Abattage {

	private static double prixAuKilos;
	private static double poidsAbattage;

	public Canard() {
		super();
	
	}

	public Canard(double poids) {
		super(poids);
		numeroIdentifiant = "CANARD" + idAutoIncrement++;
	}



	public static double getPrixAuKilos() {
		return prixAuKilos;
	}

	public static void setPrixAuKilos(double prixAuKilos) {
		Canard.prixAuKilos = prixAuKilos;
	}

	public static double getPoidsAbattage() {
		return poidsAbattage;
	}

	public static void setPoidsAbattage(double poidsAbattage) {
		Canard.poidsAbattage = poidsAbattage;
	}

	@Override
	public boolean modifPoidsAbattage(double nouveauPoidsAbattage) {

		if (nouveauPoidsAbattage > 0) {
			this.setPoidsAbattage(nouveauPoidsAbattage);
			return true;
		}
		System.out.println("Poids d'abattage incorrect !");
		return false;
	}

	@Override
	public String toString() {
		return "Canard [numeroIdentifiant=" + numeroIdentifiant + ", getPrixAuKilos()=" + getPrixAuKilos()
				+ ", getPoidsAbattage()=" + getPoidsAbattage() + ", getPoids()=" + getPoids() + "]";
	}

	@Override
	public boolean modifPrixDuJour(double NouveauPrixDuJour) {
		if (NouveauPrixDuJour>0) {
			this.setPrixAuKilos(NouveauPrixDuJour);
			return true;
		}
		System.out.println("Prix incorrect");
		return false;
	}
	

}
