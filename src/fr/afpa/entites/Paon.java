package fr.afpa.entites;

public class Paon extends Volaille {

	public Paon() {
		super();
		numeroIdentifiant = "PAON"+idAutoIncrement++;
	}

	@Override
	public String toString() {
		return "Paon [numeroIdentifiant=" + numeroIdentifiant + "]";
	}
	
	

}
