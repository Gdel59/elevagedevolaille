package fr.afpa.entites;

public class Poulet extends VolailleAAbattre implements Abattage {

	private static double prixAuKilos;
	private static double poidsAbattage;

	public Poulet(double poids) {
		super(poids);
		numeroIdentifiant = "POULET" + idAutoIncrement++;
	}

	public Poulet() {
		super();


	}

	public static double getPrixAuKilos() {
		return prixAuKilos;
	}

	public static void setPrixAuKilos(double prixAuKilos) {
		Poulet.prixAuKilos = prixAuKilos;
	}

	public static double getPoidsAbattage() {
		return poidsAbattage;
	}

	public static void setPoidsAbattage(double poidsAbattage) {
		Poulet.poidsAbattage = poidsAbattage;
	}
	

	@Override
	public boolean modifPoidsAbattage(double nouveauPoidsAbattage) {
		if (nouveauPoidsAbattage > 0) {
			this.setPoidsAbattage(nouveauPoidsAbattage);
			return true;
		}
		System.out.println("Poids d'abattage incorrect !");
		return false;
	}

	@Override
	public String toString() {
		return "Poulet [numeroIdentifiant=" + numeroIdentifiant + ", getPrixAuKilos()=" + getPrixAuKilos()
				+ ", getPoidsAbattage()=" + getPoidsAbattage() + ", getPoids()=" + getPoids() + "]";
	}

	@Override
	public boolean modifPrixDuJour(double nouveauPrixDuJour) {
		if (nouveauPrixDuJour>0) {
		this.setPrixAuKilos(nouveauPrixDuJour);
		return true;
	}
	System.out.println("Prix incorrect");
		return false;
	}



}
